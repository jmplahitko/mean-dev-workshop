#!/usr/bin/env node --harmony

'use strict';

const
    async = require('async'),
    fs = require('fs'),
    mongojs = require('mongojs'),
    database = process.argv[2],
    collection = process.argv[3],
    file = process.argv[4],
    uri = 'mongodb://localhost:27017/' + database,
    db = mongojs.connect(uri, [collection]);

let
    dbQueue = async.queue(function(doc, done) {
        db[collection].insert(doc, function(handleError, response) {
            console.log(response);
            done();
        });
    }, 10);

dbQueue.drain = closeDb;

fs.readFile(file, function(handleError, data) {
    let json = JSON.parse(data.toString());
    json.map(function(doc) {
        dbQueue.push(doc, handleError);
    });

});

function closeDb() {
    console.log('All documents have been saved to ' + database + '/' + collection);
    db.close();
};

function handleError(err) {
    console.log("error: " + err);
};